package com.silensoftware.raidhealer;

import com.badlogic.gdx.Game;

/**
 * Entry point for platforms (Android, Desktop, etc). See the create method
 * @author Andrew Jones 
 * @version 0.9 - April 2014
 *
 */
public class RaidHealer extends Game
{
	@Override
	public void create() 
	{		
		setScreen(new Play());
	}

	@Override
	public void dispose() 
	{	
		super.dispose();
	}

	@Override
	public void render() 
	{		
		super.render();
	}

	@Override
	public void resize(int width, int height)
	{
		super.resize(width, height);
	}

	@Override
	public void pause() 
	{
		super.pause();
	}

	@Override
	public void resume() 
	{
		super.resume();
	}
}
