package com.silensoftware.raidhealer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;

import com.silensoftware.raidhealer.entities.CharacterClass;
import com.silensoftware.raidhealer.entities.Dps;
import com.silensoftware.raidhealer.entities.Healer;
import com.silensoftware.raidhealer.entities.Party;
import com.silensoftware.raidhealer.entities.Tank;

/**
 * This class contains a scenerio for a fight. It deals with dealing damage
 * to the Party and taking damage from it. It also includes things such as 
 * updating and rendering related assets including health bars for the boss.
 */
public class Fight 
{
	 public static int PartyHit = 200;//Damage per attack
     public static int SmallHit = 2000;//Damage per attack
     public static float BossSwingTimer = 1.0f;//seconds, how often swing
     //private static int totalAttacks = 1;//Total attacks
     private int health;//Current health of the boss
     private int maxHealth;//Total health of the boss
     
     private Sprite bossHealthBarFull;//Sprite for the health bar
     private Sprite bossHealthBarTaken;//Sprite that appears under the health
     
     private Tank[] tanks;//All tanks
     private Dps[] dpss;//All DPS
     private Healer healer;//The healer

     private boolean gameOver;//If the game is over
     private boolean bossDead;//If the boss is dead

     private float timeSinceLastAttack;//Counts time since last attacked

     /**
      * Constructor. Instantiate everything that needs to be created straight
      * away. such as health and retrieving the tanks, dps, healer.
      * 
      * @param party The party that was created in the Play class. Used to
      * retrieve the tanks, dps, and healer.
      * @param x The x position of the health bar.
      * @param y The y position of the health bar.
      * @param modifier Each level this increases as to add difficulty.
      */
     public Fight(Party party, int x, int y, float modifier)
     {
    	 float width = UI.bossHealthBarWidth;
    	 float height = UI.bossHealthBarHeight;
    	 String full = "images/HealthBarFull.png";
    	 String base = "images/HealthBarBoss.png";
    	 
         bossDead = gameOver = false;
         tanks = party.getTanks();
         dpss = party.getDpss();
         healer = party.getHealer();
         timeSinceLastAttack = 0;//Hasn't attacked yet so  0
         
         /* Set up the top layer of the health bar. This includes texture,
           size, and position. */
         bossHealthBarFull = new Sprite(new Texture(Gdx.files.internal(full)));
         bossHealthBarFull.setSize(width, height);
         bossHealthBarFull.setPosition(x, y);
         
         /* Set up the bottom layer of the health bar. This includes texture,
         size, and position. */
         bossHealthBarTaken = new Sprite(new Texture(Gdx.files.internal(base)));
         bossHealthBarTaken.setSize(width, height);
         bossHealthBarTaken.setPosition(x, y);
         
         modifyBoss(modifier);//Make changes to the boss
     }
     
     /**
      * This is the 'difficulty' changes. Everything in here is multiplied
      * by the modifier to allow for infinite scaling of difficulties/levels
      * 
      * @param modifier
      */
     private void modifyBoss(float modifier)
     {
    	 float mod = 1 + (modifier / 10);
    	 
    	 maxHealth = health = (int)(Settings.BossHealth * mod);
    	 //BossSwingTimer /= mod;
    	 SmallHit *= mod;
    	 PartyHit *= mod;
     }

     /**
      * This method is used to update anything that requires constant
      * updating. It does basic stuff like checking for attacks and updating
      * health bars.
      */
     public void update()
     {
         timeSinceLastAttack += Gdx.graphics.getDeltaTime();
         checkPartyDead();
         nextAttack();
         updateHealthBar();
     }
     
     /**
      * This method updates the health bar. It changes the width of the 
      * bar based on a percentage of the current health and the maximum health
      */
     public void updateHealthBar()
     {
    	 float percentage = ((float)maxHealth / health);
    	 float width = bossHealthBarFull.getWidth();
    	 float height = bossHealthBarFull.getHeight();
    			
    	 bossHealthBarTaken.setSize(width / percentage, height);
     }

     /**
      * This method checks against all party members that are alive and
      * if they are all dead gameOver is set to true, otherwise nothing
      * happens.
      */
     private void checkPartyDead()
     {
         for(Tank tank:tanks)
         {
             if (tank.isAlive())
                 return;
         }
         
         for(Dps dps: dpss)
         {
             if (dps.isAlive())
                 return;
         }

         if(healer.isAlive())
             return;

         //below line never reached while a party member is alive
         gameOver = true;
     }

     /**
      * If timer is up the boss will attack.
      */
     private void nextAttack()
     {
         if(timeSinceLastAttack >= Fight.BossSwingTimer)//If time to attack
         {
             //int attack = MathUtils.random(1, totalAttacks);//Select an attack
             CharacterClass player;//The player to attack
             boolean playerHit = false;//If a player has been hit yet

             while(!playerHit)
             {
                 player = randomPlayer();//Select a player
                 //Add a switch here if more attacks are added later on
                 playerHit = smallHit(player);
             }
             timeSinceLastAttack = 0;//Set to 0 so timer starts again
         }
     }

     /**
      * One of the bosses abilities. This tries to deal a set amount of damage
      * to a player.
      * 
      * @param player This is the player that is going to take the damage
      * @return True is returned if the player was hit, otherwise false is
      * returned.
      */
     public boolean smallHit(CharacterClass player)
     {
         float dmg = (float) Fight.SmallHit;

         if(!player.isAlive())//If player is dead the attack is not done
        	 return false;//No attack was dealt
         
         player.hitFor(dmg);//Hit the player
         
         return true;//Damage was dealt
     }

     /**
      * One of the bosses abilities. This deals damage to all living players.
      */
     public void hitAll()
     {
         float dmg = (float) Fight.PartyHit;

         for(Tank tank: tanks)
         {
             if(tank.isAlive())
            	 tank.hitFor(dmg);
         }

         for(Dps dps: dpss)
         {
        	 if(dps.isAlive())
        	 	dps.hitFor(dmg);
         }

         if(healer.isAlive())
        	 healer.hitFor(dmg);
     }

     /**
      * @return Whether game is over or not
      */
     public boolean isGameOver()
     {
         return gameOver;
     }
     
     /**
      * Whether boss is dead or not
      * @return
      */
     public boolean isBossDead()
     {
    	 return bossDead;
     }

     /**
      * This returns a random player.
      * 
      * Three types of classes
      * Tanks are first, so range from 0 till their end
      * Healer(s) are last so range from partyAmount to partyAmount-healers
      * Otherwise range must be the middle group (the dps)
      * 
      * @return The randomly selected player.
      */
     public CharacterClass randomPlayer()
     {
         int index = randomPlayerIndex();

         if (index < tanks.length)
         {
        	 return tanks[index];
         }
         else if (index >= Settings.PartyAmount - Settings.HealerAmount) 
         {
             return healer;
         }
         else
         {
             return dpss[index - tanks.length];
         }
     }
     
     /**
      * Returns the index of a player, takes chance into account
      * @return The index of a random player
      */
     private int randomPlayerIndex()
     {
    	 int player;
         int chance = MathUtils.random(0, Settings.HitPartyOutOf-1);
         int tankAmount = Settings.TankAmount;

         if (chance > Settings.HitPartyChance)//greater than the chance
         {
             player = MathUtils.random(0, tankAmount - 1);
         }
         else
         {
             player = MathUtils.random(tankAmount, Settings.PartyAmount);
         }
         return player;
     }
     
     /**
      * Draws all sprites related to this class to the screen
      * 
      * @param spriteBatch The SpriteBatch to render to
      */
     public void draw(SpriteBatch spriteBatch)
     {
    	 bossHealthBarFull.draw(spriteBatch);
    	 bossHealthBarTaken.draw(spriteBatch);
     }
     
     /**
      * Draws all outlines related to this class to the screen
      * 
      * @param shapeRenderer The ShapreRenderer to render to
      */
     public void drawOutlines(ShapeRenderer shapeRenderer)
     {
    	 //bossHealthBarFull
    	 float fullX = bossHealthBarFull.getX();
    	 float fullY = bossHealthBarFull.getY();
    	 float fullWidth = bossHealthBarFull.getWidth();
    	 float fullHeight = bossHealthBarFull.getHeight();
    	 
    	 //bossHealthBarTaken
    	 float takenX = bossHealthBarTaken.getX();
    	 float takenY = bossHealthBarTaken.getY();
    	 float takenWidth = bossHealthBarTaken.getWidth();
    	 float takenHeight = bossHealthBarTaken.getHeight();
    	 
    	 shapeRenderer.rect(fullX, fullY, fullWidth, fullHeight);
    	 shapeRenderer.rect(takenX, takenY, takenWidth, takenHeight);
    	 
     }
     
     /**
      * This method deals damage to the 'boss', it is called from other classes
      * 
      * @param damage The amount of damage to take from the bosses health
      */
     public void hitFor(float damage)
     {
    	 health -= damage;
    	 if(health < 0)
    	 {
    		 health = 0;//Clamp from going past 0
    		 gameOver = true;//Game is over
    		 bossDead = true;//Boss is dead - user has won the fight
    	 }
     }
     
     /**
      * Dispose of any LibGDX classes that have a dispose method
      */
     public void dispose()
     {
    	 bossHealthBarFull.getTexture().dispose();
    	 bossHealthBarTaken.getTexture().dispose();
     }

}
