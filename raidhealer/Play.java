package com.silensoftware.raidhealer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.TimeUtils;
import com.silensoftware.raidhealer.Menu.GameMenu;
import com.silensoftware.raidhealer.entities.CharacterClass;
import com.silensoftware.raidhealer.entities.Party;
import com.silensoftware.raidhealer.entities.Spell;

/**
 * This class is the 'main' code. It makes calls to everything in the app
 * that is not handled naturally by LibGDX.
 * All the different versions supported by LibGDX make calls to this class
 * from their own main classes (Main.Java for desktop, MainActivity.java
 * for android, etc).
 * This class instantiates all the major classes that are used in the app to 
 * make the game work properly. And then chains methods such as rendering and
 * updating to other child classes.
 */
public class Play implements Screen, InputProcessor
{
	public static FreeTypeFontGenerator Generator = //used to create fonts
			new FreeTypeFontGenerator(Gdx.files.internal("fonts/calibrib.ttf"));
	private OrthographicCamera camera;//The main (and only) camera
	private SpriteBatch spriteBatch;//All sprites render to this
	private ShapeRenderer shapeRenderer;//All shapes render to this
	private Party party;//Contains all healable entities
	private Fight fight;//The current fight scenario (scales with difficulty)
	private GameMenu gameMenu;//All menus handled through this
	private Sprite background;
	private float pausedTime;//Counts the time user paused the game for
	
	/**
	 * Called from another class. Part 2 of the chaining to render
	 * all entities in the app. Makes calls to different entities
	 * depending on what should/should not be rendered.
	 * Chains draw/render methods to entities.
	 * @see com.badlogic.gdx.Screen#render(float)
	 */
	@Override
	public void render(float delta) 
	{
		update();//Everything should be updated before making calls to render
		/* 
		 * Uncomment this out if camera is ever going to move on a possible
		 * per frame basis.
		 * camera.update();
		 * spriteBatch.setProjectionMatrix(camera.combined);
		 * shapeRenderer.setProjectionMatrix(camera.combined);
		 */
		
		
		/*switch because if statement was massive due to 
		 * java syntax and looked gross
		 */
		switch(GameMenu.GAME_MENU)
		{
			case PLAYING://Playing state is in the game healing
			case WON: //Won is in the game with a message
			case LOST://Lost is in the game with a message
			case CONFIRM_BACK://Player is in game and pressed back, has message
			{
				Gdx.gl.glClearColor(0f/255, 65f/255, 55f/255, 1f);//Color to clear with
				Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);//Clear screen
				
				drawPlayingSprites();
				drawPlayingOutlines();
				drawMenu();
				
				break;//end of case
			}
			default://if not in game then in menu
			{
				drawMenu();
				break;//End of case
			}
		}//End of switch
	}

	/**
	 * Update all entities in the app if they should ever need to be updated.
	 * Chains update methods to other entities.
	 */
	private void update()
	{
		if(gameMenu.shouldRestart())
			restart(gameMenu.getDifficulty());//Restart with new scale
		
		gameMenu.update();//Update the menu
		
		if(GameMenu.GAME_MENU == GameMenu.MENU.PLAYING)//Only update if playing
		{
	        if (!fight.isGameOver())//If still fighting
	        {
	        	party.update();//Update the party (all entities)
	        	fight.update();//Update the fight (boss etc)
	        }
	        else if(fight.isBossDead())//If game is over and boss is dead
	        {
	        	gameMenu.playerWon();//Player has won
	        }
	        else//Game must be over and boss is still alive
	        {
	        	gameMenu.playerLost();//Player has lost
	        }
		}
	}
	
	/**
	 * Sets up a new party and fight
	 * Called whenever a fight needs to be created
	 * 
	 * @param difficulty Scales the difficulty. Higher = harder.
	 */
	private void restart(int difficulty)
	{
		gameMenu.restarted();
		party = new Party();
		fight = new Fight(party, UI.healthBarX, UI.healthBarY, difficulty);
		CharacterClass.SetBoss(fight);//All entities will use this fight
	}
	
	/**
	 * Any sprites that should be rendered while player is playing
	 * aka not in main menu
	 * Chains draw methods to other classes
	 */
	private void drawPlayingSprites()
	{
		spriteBatch.begin();
		//background.draw(spriteBatch);
		party.draw(spriteBatch);
		fight.draw(spriteBatch);
		spriteBatch.end();
	}
	
	/**
	 * Any outlines that should be rendered while player is playing
	 * aka not in main menu
	 * Chains drawOutlines methods to other classes
	 */
	private void drawPlayingOutlines()
	{
		shapeRenderer.begin(ShapeType.Line);//Outlines are solid lines
		shapeRenderer.setColor(Color.BLACK);//Outlines are black
		party.drawOutlines(shapeRenderer);
		fight.drawOutlines(shapeRenderer);
		shapeRenderer.end();
	}
	
	/**
	 * Draw any menus here.
	 */
	private void drawMenu()
	{
		spriteBatch.begin();
		gameMenu.draw(spriteBatch);
		spriteBatch.end();
	}

	/**
	 * Make any adjustments to the screen due to resize.
	 * @see com.badlogic.gdx.Screen#resize(int, int)
	 */
	@Override
	public void resize(int width, int height) 
	{
		camera.viewportWidth = width;
		camera.viewportHeight = height;
		camera.update();
		spriteBatch.setProjectionMatrix(camera.combined);
		shapeRenderer.setProjectionMatrix(camera.combined);
	}

	/**
	 * On create - instantiate stuff
	 * @see com.badlogic.gdx.Screen#show()
	 */
	@Override
	public void show() 
	{	
		int screenWidth = Gdx.graphics.getWidth();
		int screenHeight = Gdx.graphics.getHeight();
		FileHandle bg = Gdx.files.internal("images/Menu/Background.png");
				
		spriteBatch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();
		
		camera = new OrthographicCamera(screenWidth, screenHeight);
		camera.setToOrtho(true);//Flip y-axis so 0 is at top
	
		Gdx.input.setInputProcessor(this);//This class handles the input
		
		background = new Sprite();
		background.setTexture(new Texture(bg));
		background.setPosition(0, 0);//compensate for flip from camera
		background.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		gameMenu = new GameMenu();
	}

	/**
	 * End of app. Dispose of sprites, files etc.
	 * @see com.badlogic.gdx.Screen#hide()
	 */
	@Override
	public void hide() 
	{
		dispose();
	}

	/**
	 * When the game is paused (app minimised etc).
	 * @see com.badlogic.gdx.Screen#pause()
	 */
	@Override
	public void pause() 
	{
		pausedTime = TimeUtils.millis();//Get current time
	}

	/**
	 * @see com.badlogic.gdx.Screen#resume()
	 */
	@Override
	public void resume() 
	{
		/*
		 * There is a known bug here. Anything that uses time based scaling
		 * such as attacks, mana and health regen will continue to regen while 
		 * paused.
		 * 
		 * They regen so slow they aren't going to be taken into account yet,
		 * but cooldown will be as if the user pauses for even a short amount
		 * of time it will be noticed.
		 * 
		 */
		
		pausedTime = TimeUtils.millis() - pausedTime;//Get the difference
		pausedTime /= 1000;//Convert to seconds (same as spell timer)
		
		/*Any spells with cooldowns are compensated with pause time otherwise
		 could pause game and wait for cooldowns to reset. */
		if(party != null)//If party exists
		{
			Spell[] spells = party.getHealer().getSpells();//Get all spells
			
			for(Spell spell: spells)//Loop through each
			{
				if(spell.onCooldown())//If it is on cooldown
				{
					float time = spell.getCooldownTimer();//Get current time
					time -= pausedTime;//Increase it's time to cooldown
					spell.setCooldownTimer(time);//Change the cooldown time
				}
			}
		}//End for loop
	}

	/**
	 * Any LibGDX classes that contain dispose methods are disposed here
	 * Chains dispose methods to other classes
	 * @see com.badlogic.gdx.Screen#dispose()
	 */
	@Override
	public void dispose() 
	{
		spriteBatch.dispose();
		shapeRenderer.dispose();
		background.getTexture().dispose();
		if(party != null)
			party.dispose();
		if(fight != null)
			fight.dispose();
		if(gameMenu != null)
			gameMenu.dispose();
		
		Generator.dispose();
	}
	
//####################
//###START OF INPUT###
//####################
	/**
	 * All input for this game is handled through this one method., whether
	 * it by via touch or mouse (mobile, desktop, web all use this).
	 * Checks to see what was actually pressed.
	 * Chains touchDown methods to other classes
	 * @see com.badlogic.gdx.InputProcessor#touchDown(int, int, int, int)
	 */
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) 
	{
		if(GameMenu.GAME_MENU == GameMenu.MENU.PLAYING)//If in the game
		{
			if(button == Input.Buttons.LEFT)//Check menu and party
			{
				party.touchDown(screenX, screenY, pointer, button);
				gameMenu.touchDown(screenX, screenY, pointer, button);
				return true;//Something was touched 
			}
		}
		else//Not playing then in a menu so check menu items
		{
			if(button == Input.Buttons.LEFT)
			{
				gameMenu.touchDown(screenX, screenY, pointer, button);
				return true;//Something was touched
			}
		}
		return false;//Nothing was touched
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) 
	{return false;}
	
	@Override
	public boolean keyDown(int keycode) {return false;}

	@Override
	public boolean keyUp(int keycode) {return false;}

	@Override
	public boolean keyTyped(char character) {return false;}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) 
	{return false;}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {return false;}

	@Override
	public boolean scrolled(int amount) {return false;}
//####################
//###END OF INPUT#####
//####################
	
}
