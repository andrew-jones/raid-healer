package com.silensoftware.raidhealer.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;

import com.silensoftware.raidhealer.Settings;

/**
 * This class is an extension of the CharacterClass. The primary purpose
 * of this class is that it deals slightly more damage than other classes
 */
public class Dps extends CharacterClass
{
	/**
	 * Constructor class
	 * 
	 * @param x The x starting position of the new character
     * @param y The y starting position of the new character
	 */
    public Dps(float x, float y) 
	{
    	super(x, y);
    	FileHandle healthBar = Gdx.files.internal("images/HealthBarDps.png");
	    maxHealth = Settings.DpsHealth;
	    health = (float)maxHealth;
	    damagePerSecond = (int)(Settings.DamageDealtPerSecond);
	    healthRegen = Settings.HealthRegen;
	    missAttackChance = 0;
	    healthBarTaken.setTexture(new Texture(healthBar));
	}
}
