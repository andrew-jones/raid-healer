package com.silensoftware.raidhealer.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.silensoftware.raidhealer.Settings;
import com.silensoftware.raidhealer.UI;

/**
 * This class is an extension of the CharacterClass. The primary purpose
 * of this class is that it takes slightly less damage than other classes
 */
public class Tank extends CharacterClass
{
	/*
	 * Constructor
	 * 
	 * @param x The x starting position of the new character
	 * @param y The y starting position of the new character
	 */
    public Tank(float x, float y) 
    {
    	super(x, y);
    	
    	float damage = Settings.DamageDealtPerSecond;
    	float modifier = Settings.TankDamageDealtModifier;
    	FileHandle handle = Gdx.files.internal("images/HealthBarTank.png");
    	
	    maxHealth = Settings.TankHealth;
	    health = (float)maxHealth;
	    damagePerSecond = (int) (damage * modifier);
	    healthRegen = 15;//Settings.HealthRegen;
	    damageTakenModifier = Settings.TankDamageTakenModifier;
	    missAttackChance = 0;
	    healthBarTaken.setTexture(new Texture(handle));
	    damageDoneModifier = Settings.TankDamageDealtModifier;
    }
}
