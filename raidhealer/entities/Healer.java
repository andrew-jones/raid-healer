package com.silensoftware.raidhealer.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.silensoftware.raidhealer.Settings;
import com.silensoftware.raidhealer.UI;

/**
 * This class is the 'player' class
 * It controls who gets healed, what spells are used
 * cast times and healing amount of spells etc
 */
public class Healer extends CharacterClass
{	
	 private boolean casting;//Whether casting or not
	 
	 private Sprite castBarBase, //Castbar base
	 	castBarOverlay,//Overlay of castbar
	 	spellSelectedOverlay,//Overlay for selected spell
	 	manaBarBase, //Mana bar base
	 	manaBarLeft;//Mana left
	 private int currentSpellIndex;
	 private float castTime;//Amount of time casting, percent for overlay draw
	 private float currentSpellCastTime;//Current spells cast time
	
	 private float maxMana;//Maximum amount of mana the player has
	 private float currentMana;//Current amount of mana the player has
	
	 private CharacterClass playerToHeal;//Which player to heal when cast done
	 
	 private FreeTypeFontGenerator generator;//Used for custom fonts
	 private BitmapFont calibrib;//Used to render fonts
	 private Vector2 castMessageBounds;//Render bounds for messages
	 private String cancelCastMessage = "Cancel cast.";//Display when casting
	
	 private Spell[] spells;//all the spells of the healer
	 private Spell currentSpell;//The current spell

	 /**
	  * Constructor. Set up anything that needs to be set at run time
	  * x is the pixels left that the cast bar should start at
	  * y is the pixels down that the cast bar should start at
	  * Other bars from healer are dynamically set depending on these positions
	  * 
	  * @param x Xpos of the health bar
	  * @param y Ypos of the health bar
	  */
	 public Healer(float x, float y)
	 {
		 super(x, y);
		 //Location of textures
		
		 FileHandle healer = Gdx.files.internal("images/HealthBarHealer.png");
		 
		 healthBarTaken.setTexture(new Texture(healer));//Create healthbar
		 
		 //Set up and position things
		 setUpStats();
		 setUpManaBar();
	     setUpSpells();
	     setUpCastBar();
		 setUpFonts();

	 }
	 
	 /**
	  * Set up fonts. Load fonts, set bounds, set positions
	  */
	 private void setUpFonts()
	 {
		 FileHandle font = Gdx.files.internal("fonts/calibrib.ttf");
		 float textWidth, textHeight, centerX, centerY, messageX, messageY;
		 
		 generator = new FreeTypeFontGenerator(font);
		 calibrib = generator.generateFont((int)(UI.fontSize / 1.2f));
		 calibrib.setColor(new Color(1, 1, 1, 0.5f));
		 calibrib.setScale(1, -1);
		 
		 textWidth = calibrib.getBounds(cancelCastMessage).width;
		 centerX = (castBarBase.getWidth() / 2) - (textWidth / 2);
		 messageX = castBarBase.getX() + centerX;
		 textHeight = calibrib.getBounds(cancelCastMessage).height;
		 centerY = (castBarBase.getHeight() / 2) + (textHeight / 2);
		 messageY =  castBarBase.getY() + centerY;
		 
		 castMessageBounds = new Vector2(messageX, messageY);
	 }
	 
	 /**
	  * Set dimensions and positions of the cast bar base and overlay
	  */
	 private void setUpCastBar()
	 {
		 FileHandle cast = Gdx.files.internal("images/CastBar.png");
		 FileHandle castOver = Gdx.files.internal("images/CastBarOverlay.png");
		 
		 float spellY = spells[0].getSpellBase().getY();
	     float height = spells[0].getSpellBase().getHeight();
	     float width = spells[0].getSpellBase().getWidth() * spells.length;
	     
	     castBarBase = new Sprite(new Texture(cast));
		 castBarOverlay = new Sprite(new Texture(castOver));
		 
	     castBarBase.setPosition(UI.healthBarX, spellY);
		 castBarBase.setSize(width, height);
		 
		 castBarOverlay.setPosition(UI.healthBarX, spellY);
		 castBarOverlay.setSize(0, height);//Width changed later
	 }
	 
	 /**
	  * Set dimensions and positions of the mana bar base and overlay
	  */
	 private void setUpManaBar()
	 {
		 FileHandle mana = Gdx.files.internal("images/ManaBarBase.png");
		 FileHandle manaOver = Gdx.files.internal("images/ManaBarLeft.png");
		 
		 float healthY = healthBarFull.getY();
		 float healthHeight = healthBarFull.getHeight();
		 float manaY = healthY + healthHeight + UI.groupGap;
		 
		 manaBarBase = new Sprite(new Texture(mana));
		 manaBarBase.setSize(UI.manaBarWidth, UI.manaBarHeight);
		 manaBarBase.setPosition(UI.healthBarX, manaY);
		 
		 manaBarLeft = new Sprite(new Texture(manaOver));
	     manaBarLeft.setSize(UI.manaBarWidth, UI.manaBarHeight);
	     manaBarLeft.setPosition(UI.healthBarX, manaY);
	 }
	 
	 /**
	  * Instantiate stats for the healer
	  */
	 private void setUpStats()
	 {
	     maxHealth = Settings.HealerHealth;
	     health = (float) maxHealth;
	     healthRegen = Settings.HealthRegen;
	     missAttackChance = 0;
		 currentSpellIndex = 0;
		 casting = false;
		 currentMana = maxMana = Settings.HealerMaxMana;
	 }
	 
	 /**
	  * Instantiate spells and set their positions, as well as set cast
	  * times, mana amount etc
	  */
	 private void setUpSpells()
	 {
		 spells = new Spell[5];
		 
		 float startX = manaBarBase.getX();
		 float startY = manaBarBase.getY() + manaBarBase.getHeight();
		 float spellX, castTime, barX, barY;
		 int healFor, manaCost;
		 String locaction = "images/Spell.png";
		 FileHandle sprite = Gdx.files.internal("images/SpellOverlay.png");
		 
		 for(int i = 0; i < spells.length; i++)
		 {
			 spellX = startX + (i * UI.spellWidth);
			 healFor = 1000 * (i + 1);
			 castTime = 0.5f * (i + 1);
			 manaCost = 2 * (i + 1);
			 
			 spells[i] = new Spell(locaction, spellX, startY, 
					 healFor, castTime, manaCost);
		 }
		 
		 spells[0].setManaCost(1.2f);//Cheap crappy spell
		 
		 spells[1].setCastTime(0.25f);//second spell is fast
		 spells[1].setHealFor(3000);//heals for a lot as well
		 
		 spells[2].setHealFor(5000);//third spell heals for a lot, is slower
		 
		 spells[spells.length-2].setHotSpell(5, 400);//Second last is HoT
		 
		 //last spell is cooldown
		 spells[spells.length-1].setHealAll(true);
		 spells[spells.length-1].setCastTime(0);
		 spells[spells.length-1].setManaCost(5);
		 spells[spells.length-1].setCooldown(10);
		 spells[spells.length-1].setHealFor(2000);
				 
		 currentSpell = spells[0];

		 barX = currentSpell.getSprite().getX();
		 barY = currentSpell.getSprite().getY();
		 
	     spellSelectedOverlay = new Sprite(new Texture(sprite));
	     spellSelectedOverlay.setSize(UI.spellWidth, UI.spellWidth);
	     spellSelectedOverlay.setPosition(barX, barY);
	 }
	
	 /**
	  * Start of a basic heal. When player clicks a character it is passed
	  * here and the cast starts.
	  * 
	  * Sets the cast timer if not currently casting
	  * 
	  * @param player The player to heal after cast is complete
	  */
	 public void healPlayer(CharacterClass player)
	 {
		 boolean hasMana = currentMana > currentSpell.getManaCost();
		 boolean onCooldown = currentSpell.onCooldown();
		 
	     if(alive && !casting && hasMana && !onCooldown)
	     {
	    	 currentSpellCastTime = currentSpell.getCastTime();
	    	 castTime = 0;
	    	 casting = true;
	    	 playerToHeal = player;
	     }
	 }
	 
	 /**
	  * Once the cast time is up this method gets called and heals
	  * the player that was passed into healPlayer() for the amount
	  * of the currently selected spells heal amount
	  */
	 public void heal()
	 {
		 if(alive && playerToHeal.isAlive())//may die during the cast
		 {
			 playerToHeal.healedFor(currentSpell.getHealFor());
			 currentMana -= currentSpell.getManaCost();
			 if(currentMana < 0)
				 currentMana = 0;
			 currentSpell.startCooldown();
		 }
	 }
	 
	 /**
	  * Heals all entities within the characters parameter
	  * 
	  * @param characters List of characters to heal
	  */
	 public void healAll(CharacterClass[] characters)
	 {
		 boolean onCooldown = currentSpell.onCooldown();
		 boolean hasMana = currentMana > currentSpell.getManaCost();
		 
		 if(alive && hasMana && !onCooldown)
		 {
			 for(CharacterClass character: characters)
			 {
				 if(character.isAlive())
				 {
					 character.healedFor(currentSpell.getHealFor());
				 }
			 }
			 currentMana -= currentSpell.getManaCost();
			 currentSpell.startCooldown();
		 }
	 }
	 
	 /**
	  * Make a call here when want to use a heal over time spell
	  * It is presumed that HoT spell is instant cast
	  * 
	  * @param player The player to apply the HoT to
	  */
	 public void healHot(CharacterClass player)
	 {
		 boolean hasMana = currentMana > currentSpell.getManaCost();
		 boolean onCooldown = currentSpell.onCooldown();
		 int hotHealFor;
		 
		 if(alive && hasMana && !onCooldown)
		 {
			 hotHealFor = currentSpell.getHotHealPerSec();
			 player.applyHot(currentSpell.getHotTime(), hotHealFor);
			 currentMana -= currentSpell.getManaCost();
			 currentSpell.startCooldown();
		 }
	 }
	 
	 /**
	  * Called once per frame, updates anything that doesn't change on event
	  * @see entities.CharacterClass#update()
	  */
	 public void update()
	 {
		 super.update();
		 updateCast();
		 updateManaBar();
		 
		 for(Spell spell: spells)
			 spell.update(currentMana);
		 
		 for(int i = 0; i < spells.length; i++)
		 {
			 if(!currentSpell.getEnoughMana())//Deselect spell if not enough
			 {
				 currentSpellIndex++;//Shift to next spell
				 if(currentSpellIndex >= spells.length)
					 currentSpellIndex = 0;
				 setActiveSpell(currentSpellIndex);
			 }
			 else
				 break;//found the selected spell
		 }//want to keep looping as new spell may not have enough mana either
	 }
	 
	 /**
	  * Updates the mana bar, changing the amount of mana
	  * shown depending on the mana the healer has
	  */
	 private void updateManaBar()
	 {
		 float percent = maxMana / currentMana;
		 float width = manaBarBase.getWidth() / percent;
		 
		 manaBarLeft.setSize(width, manaBarLeft.getHeight());//y
		 currentMana += Settings.HealerManaRegen * Gdx.graphics.getDeltaTime();
		 
		 if(currentMana > maxMana)
			 currentMana = maxMana;
	 }
	 
	 /**
	  * Draws the overlay over the base, does so in percentage style format
	  * so that if the player is 25% through cast, 1/4th is taken etc
	  * Once cast is done it's set to 0 and casting stops
	  */
	 private void updateCast()
	 {
		 if(casting)
		 {
			 float percent = currentSpellCastTime / castTime;
			 float width = castBarBase.getWidth() / percent;
			 
			 castTime += Gdx.graphics.getDeltaTime();//add to the time
			 castBarOverlay.setSize(width, castBarOverlay.getHeight());
			 
			 if(castTime > currentSpellCastTime)
			 {
				 heal();
				 casting = false;
				 castTime = 0;
				 castBarOverlay.setSize(0, castBarOverlay.getHeight());
			 }
		 }
	 }
	 
	 /**
	  * All drawing of this class goes here
	  * Call super so the extended class can draw
	  * Draws either castbar, or spells, but not both
	  * 
	  * @param spriteBatch The sprite batch to render to
	  * @see entities.CharacterClass#draw()
	  */
	 public void draw(SpriteBatch spriteBatch)
	 {
		 super.draw(spriteBatch);
		 
		 if(casting)//if casting draw castbar
		 {
			 castBarBase.draw(spriteBatch);
			 castBarOverlay.draw(spriteBatch);
			 calibrib.draw(spriteBatch, cancelCastMessage, //draw message
					 castMessageBounds.x, castMessageBounds.y);
		 }
		 else//otherwise draw the spells
		 {
			 for(Spell spell: spells)
				 spell.draw(spriteBatch);
			 
			 spellSelectedOverlay.draw(spriteBatch);
		 }
		 manaBarBase.draw(spriteBatch);
		 manaBarLeft.draw(spriteBatch);
	 }
	 
	 /**
	  * Draw the outlines for the sprites
	  * 
	  * @param shapeRenderer The shape renderer to use
	  * @see entities.CharacterClass#drawOutlines()
	  */
	 public void drawOutlines(ShapeRenderer shapeRenderer)
	 {
		 super.drawOutlines(shapeRenderer);
		 
		 //Mana base
		 float manaX = manaBarBase.getX();
		 float manaY = manaBarBase.getY();
		 float manaW = manaBarBase.getWidth();
		 float manaH = manaBarBase.getHeight();
		 //mana left
		 float leftX = manaBarLeft.getX();
		 float leftY = manaBarLeft.getY();
		 float leftW = manaBarLeft.getWidth();
		 float leftH = manaBarLeft.getHeight();
		 
		 if(casting)
		 {
			 //base
			 float baseX = castBarBase.getX();
			 float baseY = castBarBase.getY();
			 float baseW = castBarBase.getWidth();
			 float baseH = castBarBase.getHeight();
			 //overlay
			 float overX = castBarOverlay.getX();
			 float overY = castBarOverlay.getY();
			 float overW = castBarOverlay.getWidth();
			 float overH = castBarOverlay.getHeight();
			 
			 shapeRenderer.rect(baseX, baseY, baseW, baseH);
			 shapeRenderer.rect(overX, overY, overW, overH);
		 }
		 else
		 {
			 for(Spell spell: spells)//each spell
				 spell.drawOutlines(shapeRenderer);
		 }
		 shapeRenderer.rect(manaX, manaY, manaW, manaH);
		 shapeRenderer.rect(leftX, leftY, leftW, leftH);
	 }
	 
	 /**
	  * Get a list of all spells, used by other classes
	  * 
	  * @return List of spells
	  */
	 public Spell[] getSpells()
	 {
		 return spells;
	 }
	 
	 /**
	  * Pass in what spell has been selected (judging by what was pressed)
	  * and then set that spell to the current spell (ready for casting)
	  * and then set the overlay to that spell to show it is selected
	  * 
	  * @param i The spell to set as the active spell
	  */
	 public void setActiveSpell(int i)
	 {
		 float x, y;
		 currentSpellIndex = i;
		 currentSpell = spells[currentSpellIndex];
		 
		 x = currentSpell.getSprite().getX();
		 y = currentSpell.getSprite().getY();
	     spellSelectedOverlay.setPosition(x, y);//Update spell overlay
	 }
	 
	 /**
	  * Pass in the character to be healed and set playerToHeal to that
	  * 
	  * @param c Set the player to heal
	  */
	 public void setPlayerToHeal(CharacterClass c)
	 {
		 playerToHeal = c;
	 }
	 
	 /**
	  * Call any libgdx dispose methods used in this class
	  * @see entities.CharacterClass#dispose()
	  */
	 public void dispose()
	 {
		 super.dispose();
		 castBarBase.getTexture().dispose();
		 castBarOverlay.getTexture().dispose();
		 spellSelectedOverlay.getTexture().dispose();
		 manaBarBase.getTexture().dispose();
		 manaBarLeft.getTexture().dispose();
	 }
	 
	 /**
	  * Get the current spell index
	  * 
	  * @return the current spell index
	  */
	 public int getCurrentSpellIndex()
	 {
		 return currentSpellIndex;
	 }

     /**
      * Whether the healthBar contains a point
      * Bounding box 'collision'
      * 
      * @param x The horizontal position of the 'touch'
      * @param y The vertical position of the 'touch'
      * @return Whether the bar contains the x and y pos
      */
     public boolean castBarContains(int x, int y)
     {
    	 boolean xMin = castBarBase.getX() <= x;
    	 boolean xMax = castBarBase.getX() + castBarBase.getWidth() >= x;
    	 boolean yMin = castBarBase.getY() <= y;
    	 boolean yMax = castBarBase.getY() + castBarBase.getHeight() >= y;
    	 
   	  	 return xMin && xMax && yMin && yMax;
     }
     
     /**
      * Whether the player is casting a spell or not
      * 
      * @return Whether player is casting
      */
     public boolean isCasting()
     {
    	 return casting;
     }
     
     /**
      * Stops the user from casting
      */
     public void cancelSpell()
     {
    	 castTime = 0;//reset the spell timer back to 0
    	 casting = false;//stop casting
    	 castBarOverlay.setSize(0, castBarOverlay.getHeight());
     }
     
     /**
      * Will return the current mana the healer has.
      * 
      * @return Current amount of mana healer has
      */
     public float getCurrentMana()
     {
    	 return currentMana;
     }
     
     /**
      * Set the amount of mana the healer currently has.
      * 
      * @param mana CurrentMana will become this amount
      */
     public void setCurrentMana(float mana)
     {
    	 currentMana = mana;
     }

}
