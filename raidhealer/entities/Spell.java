package com.silensoftware.raidhealer.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.silensoftware.raidhealer.Play;
import com.silensoftware.raidhealer.UI;

/**
 * The healer can have one, or many different spells. They are used to heal
 * a player (or all at once) and contain many different variables as shown
 * in this class.
 */
public class Spell 
{
	private int healFor;//How much the spell should heal for
	private float castTime;//How long to cast before healing
	private float manaCost;//How much mana the spell should cost
	private Sprite spellBase;//Sprite for the spell
	private Sprite darkOverlay;//Displayed when not enough mana or on CD
	private boolean enoughMana;//this false means draw overlay
	private boolean healAll;//Whether spell heals all
	private float cooldown;//time for cooldown
	private float cooldownTimer;//time since - in seconds
	
	private boolean hot;//is spell heal over time
	private float hotTime;//hot long to heal for
	private int hotHealPerSec;//how much to heal per second
	
	private BitmapFont calibrib;//Renders fonts
	private Vector2 cooldownTimerBounds;//Where to draw the timer
	
	/**
	 * Constructor. Set up sprites and other variables
	 * 
	 * @param location Location of the texture for spell
	 * @param x Xpos of the spell sprite
	 * @param y Ypos of the spell sprite
	 * @param healFor How much to heal for
	 * @param castTime How long the cast time is
	 * @param manaCost How much mana spell costs
	 */
	public Spell(String location, float x, float y, 
			int healFor, float castTime, int manaCost)
	{
		String blackLocation = "images/Colours/BlackTransparent.png";
		FileHandle black = Gdx.files.internal(blackLocation);
		
		spellBase = new Sprite(new Texture(Gdx.files.internal(location)));
		spellBase.setPosition(x, y);
		spellBase.setSize(UI.spellWidth, UI.spellHeight);
		
		darkOverlay = new Sprite(new Texture(black));//no mana/cd
		darkOverlay.setPosition(x, y);
		darkOverlay.setSize(UI.spellWidth, UI.spellHeight);
		
		enoughMana = false;
		
		hot = false;//set to false, call setHotSpell for changes
		
		this.healFor = healFor;
		this.castTime = castTime;
		this.manaCost = manaCost;
		healAll = false;
		cooldown = 0;
		cooldownTimer = 0;
	}
	
	/**
	 * Called once per frame
	 * Checks to see if healer has enough mana for this spell
	 * 
	 * @param currentMana Amount of mana the healer has
	 */
	public void update(float currentMana)
	{
		if(cooldown != 0)//if there is a cooldown
		{
			cooldownTimer += Gdx.graphics.getDeltaTime();//Update the timer
		}
		if(currentMana >= manaCost)
		{
			enoughMana = true;
		}
		else
		{
			enoughMana = false;
		}
	}
	
	/**
	 * Render all sprites, called per frame
	 * 
	 * @param spriteBatch The sprite batch to render to
	 */
	public void draw(SpriteBatch spriteBatch)
	{
		spellBase.draw(spriteBatch);
		
		if(onCooldown())
		{
			//Format to two decimal place
			String message = String.format("%.2f", cooldown - cooldownTimer);
			float x = cooldownTimerBounds.x;
			float y = cooldownTimerBounds.y;
			
			darkOverlay.draw(spriteBatch);
			calibrib.draw(spriteBatch, message, x, y);
		}
		else if(!enoughMana)
		{
			darkOverlay.draw(spriteBatch);
		}
	}
	
	/**
	 * Render all outlines
	 * 
	 * @param shapeRenderer The shape renderer to render to
	 */
	public void drawOutlines(ShapeRenderer shapeRenderer)
	{
		float width = spellBase.getWidth();
		float height = spellBase.getHeight();
		
		shapeRenderer.rect(spellBase.getX(), spellBase.getY(), width, height);
	}
	
	/**
	 * Whether the spell is instant cast or not
	 * @return Whether spell is instant
	 */
	public boolean isInstantCast()
	{
		return castTime == 0;
	}
	
	/**
	 * Cast time of the spell
	 * @return The current cast time of spell
	 */
	public float getCastTime()
	{
		return castTime;
	}
	
	/**
	 * Sets the current cast time of spell
	 * 
	 * @param c The cast time the spell should have
	 */
	public void setCastTime(float c)
	{
		castTime = c;
	}
	
	/**
	 * Get how much the spell heals for
	 * @return The amount the spell heals for
	 */
	public int getHealFor()
	{
		return healFor;
	}
	
	/**
	 * Set how much the spell heals for
	 * 
	 * @param h The amount the spell should heal for
	 */
	public void setHealFor(int h)
	{
		healFor = h;
	}
	
	/**
	 * Get how much mana the spell costs to cast
	 * @return The amount of mana the spell uses
	 */
	public float getManaCost()
	{
		return manaCost;
	}
	
	/**
	 * Sets the amount of mana the spell costs to use
	 * 
	 * @param m The amount of mana the spell should use
	 */
	public void setManaCost(float m)
	{
		manaCost = m;
	}
	
    /**
     * Whether the box contains a point
     * Bounding box 'collision'
     * 
     * @param x The horizontal position of the point
     * @param y The vertical position of the point
     * @return Whether the box contains the point
     */
    public boolean contains(int x, int y)
    {
  	  return spellBase.getX() <= x 
  			  && spellBase.getX() + spellBase.getWidth() >= x 
  			  && spellBase.getY() <= y 
  			  && spellBase.getY() + spellBase.getHeight() >= y;
    }
    
    /**
     * Returns the sprite of the spell
     * @return The sprite of the spell
     */
    public Sprite getSprite()
    {
    	return spellBase;
    }
    
    /**
     * Whether the spell heals everyone or not
     * @return If spell heals all
     */
    public boolean isHealAll()
    {
    	return healAll;
    }
    
    /**
     * Set whether the spell should heal all or not
     * 
     * @param b Whether the spell should heal all
     */
    public void setHealAll(boolean b)
    {
    	healAll = b;
    }
    
    /**
     * Set how long the spell goes on cooldown after being used
     * Also sets up fonts as if there is no cooldown being used then
     * spell does not need a font
     * 
     * @param c The time the spell should be on cooldown for
     */
    public void setCooldown(float c)
    {
    	String cooldownTimerMessage = "0.00";
    	
    	calibrib = Play.Generator.generateFont((int)(UI.fontSize / 1.2f));
		calibrib.setColor(new Color(1, 1, 1, 0.5f));
		calibrib.setScale(1, -1);
		
    	float fontCenterX = calibrib.getBounds(cooldownTimerMessage).width / 2;
    	float fontCenterY = calibrib.getBounds(cooldownTimerMessage).height / 2;
    	
    	float x = spellBase.getX() + (spellBase.getWidth() / 2) - fontCenterX;
    	float y = spellBase.getY() + (spellBase.getHeight() / 2) + fontCenterY;
    	
    	cooldown = c;
    	cooldownTimer = c;
    	
    	cooldownTimerBounds = new Vector2(x, y);//assuming always 0.00 format
    }
    
    /**
     * Get the cooldown of the spell
     * @return The cooldown of spell
     */
    public float getCooldown()
    {
    	return cooldown;
    }
    
    /**
     * Get the cooldown timer of the spell
     * @return The cooldown timer of spell
     */
    public float getCooldownTimer()
    {
    	return cooldownTimer;
    }
    
    /**
     * Set the cooldown timer
     * 
     * @param time The amount of time spell is on cooldown for
     */
    public void setCooldownTimer(float time)
    {
    	cooldownTimer = -time;
    }
    
    /**
     * On cooldown if the timer is less than the required time
     * @return If spell is on cooldown
     */
    public boolean onCooldown()
    {
    	return cooldownTimer < cooldown;
    }
    
    /**
     * When spell is cast this is called so start the timer
     */
    public void startCooldown()
    {
    	cooldownTimer = 0;
    }
    
    /**
     * Get the spell base sprite
     * @return The spell base
     */
    public Sprite getSpellBase()
    {
    	return spellBase;
    }
    
    /**
     * Healer will change this if they have enough mana or not
     * 
     * @param b Whether there is enough mana or not
     */
    public void setEnoughMana(boolean b)
    {
    	enoughMana = b;
    }
    
    /**
     * Whether there is enough mana or not
     * @return Whether there is enough mana
     */
    public boolean getEnoughMana()
    {
    	return enoughMana;
    }
    
    /**
     * Sets the spell as a heal over time that can be placed on player
     * that lasts for X seconds and heals Y health per second
     * 
     * @param time The time the HoT should last for
     * @param perSecond The amount to heal per second
     */
    public void setHotSpell(float time, int perSecond)
    {
    	hot = true;
    	hotTime = time;
    	hotHealPerSec = perSecond;
    	castTime = 0;
    } 
    
    /**
     * Get how long the HoT lasts for
     * @return The time HoT lasts for
     */
    public float getHotTime()
    {
    	return hotTime;
    }
    
    /**
     * Get how much the HoT heals for per second
     * @return Amount HoT heals for per second
     */
    public int getHotHealPerSec()
    {
    	return hotHealPerSec;
    }
    
    /**
     * Whether spell is HoT or not
     * @return Whether spell is HoT
     */
    public boolean isHot()
    {
    	return hot;
    }
    
    /**
     * Set whether spell is a HoT or not
     * 
     * @param b Whether spell is a HoT
     */
    public void setHot(boolean b)
    {
    	hot = b;
    }
}
