package com.silensoftware.raidhealer.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import com.silensoftware.raidhealer.Fight;
import com.silensoftware.raidhealer.Settings;
import com.silensoftware.raidhealer.UI;
import com.silensoftware.raidhealer.Play;

/**
 * All three classes extend from this class. It contains basics such as 
 * rendering, updating, etc.
 */
public class CharacterClass 
{
	  protected boolean alive;//if player is alive or not
      protected float health;//current amount of health
      protected int maxHealth;//maximum amount of health
      protected int damagePerSecond;//how much damage per second should do
      protected float healthRegen;//how much health per second should regen
      protected float damageTakenModifier;//how much extra/less damage to take
      protected float damageDoneModifier;//how much extra/less damage to deal
      protected int missAttackChance;//chance to miss
      protected float lastAttack;//reset to 0, more than swingTimer to attack
      protected float swingTimer;//how often the character can swing
      
      protected boolean hotApplied;//has HoT spell onself
      protected float hotLastingTime;//how long to last for in seconds
      protected int hotHealPerSecond;//how much to heal per second
      protected float hotTimer;//starts from 0 and goes till hotLastingTime
      protected Sprite hotIcon;//The icon to draw when character has HoT on them

      protected Sprite healthBarFull;//full size of health bar
      protected Sprite healthBarTaken;//health remaining bar overlayed

      protected float healthX;//top left of health bar
      protected float healthY;//top left of health bar
      protected static Play window;//used to draw to, and keep mouse position
      protected static Vector2 mouse;//the mouse/finger, contains info on press
      
      protected static Fight boss;//the boss to attack

      /**
       * Constructor. Instantiate everything that needs to be up straight away
       * such as health bars textures, positions and width.
       * This also sets the defaults of each class, which may be changed
       * later on in each classes constructors.
       * 
       * @param x The x starting position of the new character
       * @param y The y starting position of the new character
       */
      public CharacterClass(float x, float y)
      {
    	  FileHandle full = Gdx.files.internal("images/HealthBarFull.png");
    	  FileHandle noTexture = Gdx.files.internal("images/NoTexture.png");
    	  FileHandle hot = Gdx.files.internal("images/HoT.png");
    	  
    	  float width = UI.fullHealthBarWidth;
    	  float height = UI.fullHealthBarHeight;
    	  //Position of hotX is near the right of healthBars end
    	  float hotX = (x + width) - (UI.hotIconWidth + UI.hotIconXGap);
          float hotY = y  + ((height - UI.hotIconHeight) / 2);//center
    	  
          alive = true;
          health = damagePerSecond = missAttackChance = 0;
          damageDoneModifier = damageTakenModifier = 1.0f;
          healthRegen = lastAttack = 0;
          swingTimer = Settings.SwingTimer;

          healthX = x;
          healthY = y;
          healthBarFull = new Sprite(new Texture(full));
          healthBarFull.setBounds(x, y, width, height);

          healthBarTaken = new Sprite(new Texture(noTexture));
          healthBarTaken.setBounds(x, y, width, height);
          
          hotIcon = new Sprite(new Texture(hot));
          hotIcon.setBounds(hotX, hotY, UI.hotIconWidth, UI.hotIconHeight);
          
          hotApplied = false;
      }

      /**
       * Returns a characters health
       * @return The characters health
       */
      public float getHealth()
      {
          return health;
      }

      /**
       * Used to update anything that may need to be changed every frame
       */
      public void update()
      {
    	  if(alive)
    	  {
	          regenHealth();
	          if(hotApplied)
	        	  updateHot();
	          updateHealthBar();
	          attack();
    	  }
      }
      
      /**
       * Deals damage to a boss if it has been x amount of time since last
       */
      public void attack()
      {
    	  int damage = Settings.DamageDealtPerSecond ;
    	  lastAttack += Gdx.graphics.getDeltaTime();//Time since last frame
    	  
    	  if(lastAttack > swingTimer)
    	  {
    		  boss.hitFor((damage * damageDoneModifier) / Settings.SwingTimer);
    		  lastAttack = 0;
    	  }
      }
      
      /**
       * Render sprites for the character.
       * @param spriteBatch Where to render to
       */
      public void draw(SpriteBatch spriteBatch)
      {
    	  healthBarFull.draw(spriteBatch);
    	  healthBarTaken.draw(spriteBatch);
    	  
    	  if(hotApplied)
    		  hotIcon.draw(spriteBatch);
      }
      
      /**
       * Draw the outlines for the character.
       * @param shapeRenderer Where to render to
       */
      public void drawOutlines(ShapeRenderer shapeRenderer)
      {
    	  float fullX = healthBarFull.getX();
    	  float fullY = healthBarFull.getY(); 
    	  float fullWidth = healthBarFull.getWidth();
    	  float fullHeight = healthBarFull.getHeight();
    	  
    	  float takenX = healthBarTaken.getX();
    	  float takenY = healthBarTaken.getY(); 
    	  float takenWidth = healthBarTaken.getWidth();
    	  float takenHeight = healthBarTaken.getHeight();
    	  
    	  shapeRenderer.rect(fullX, fullY, fullWidth, fullHeight);
    	  shapeRenderer.rect(takenX, takenY, takenWidth, takenHeight);  
      }

      /**
       * The width of health bars change depending on how much health the
       * character has
       */
      private void updateHealthBar()
      {
    	  float percentage = (float)maxHealth / (float)health;
    	  float width = healthBarFull.getWidth() / percentage;
    	  float height = healthBarFull.getHeight();
    	  
    	  healthBarTaken.setSize(width, height);
      }
      
      /**
       * Continue to heal the player, or remove HoT if time is up
       */
      private void updateHot()
      {
    	  hotTimer += Gdx.graphics.getDeltaTime();
    	  if(hotTimer >= hotLastingTime)
    	  {
    		  hotApplied = false;//remove hot icon
    	  }
    	  else
    	  {
              health += hotHealPerSecond * Gdx.graphics.getDeltaTime();
              if (health > maxHealth)
                  health = maxHealth;
    	  }    
      }

      /**
       * The character takes damage and loses health. If they are hit below
       * 1 health they are dead
       * @param dmg The amount of damage to hit the character for.
       */
      public void hitFor(float dmg)
      {
          health -= dmg / damageTakenModifier;//Some classes will take less
          if (health <= 0)
          {
              health = 0;
              alive = false;
              hotApplied = false;//remove any hots on player
              updateHealthBar();//final call to update once dead
          }
      }

      /**
       * The character gains some health (usually per second)
       */
      public void regenHealth()
      {
          health += healthRegen * Gdx.graphics.getDeltaTime();//Scales with fps
          if (health > maxHealth)//Cant gain extra health
              health = maxHealth;
      }

      /**
       * Heal the character for an amount
       * @param heal The amount to heal for
       */
      public void healedFor(int healAmount)
      {
          health += healAmount;
          if (health > maxHealth)//Cant gain extra health
              health = maxHealth;
      }
      
      /**
       * Add a heal over time to the character
       * 
       * @param lastForInSeconds How long it should last
       * @param perSecond How much it should heal over 1 second
       */
      public void applyHot(float lastForInSeconds, int perSecond)
      {
          hotLastingTime = lastForInSeconds;
          hotHealPerSecond = perSecond;
          hotTimer = 0;//refresh
    	  hotApplied = true;
      }
      
      /**
       * Sets the boss that all characters will attack
       * @param f The Fight that the boss is contained in
       */
      public static void SetBoss(Fight f)
      {
    	  boss = f;
      }

      /**
       * Whether the player is alive or not
       * @return Boolean if character is alive
       */
      public boolean isAlive()
      {
          return alive;
      }
      
      /**
       * Whether the healthBar contains a point. If all are true then the 
       * position must have been within the bounding box.
       * Bounding box 'collision'
       * 
       * @param x The horizontal position of the input
       * @param y The vertical position of the input
       * @return Whether the input was in this character or not.
       */
      public boolean healthBarContains(int x, int y)
      {
    	  boolean xMin = healthBarFull.getX() <= x;
    	  boolean xMax = healthBarFull.getX() + healthBarFull.getWidth() >= x;
    	  boolean yMin = healthBarFull.getY() <= y ;
    	  boolean yMax = healthBarFull.getY() + healthBarFull.getHeight() >= y;
    	  
    	  return xMin && xMax && yMin && yMax;
      }
      
      /**
       * Correctly dispose of all LibGDX classes that contain a dispose method
       */
      public void dispose()
      {
    	  healthBarFull.getTexture().dispose();
    	  healthBarTaken.getTexture().dispose();
    	  hotIcon.getTexture().dispose();
      }
}
