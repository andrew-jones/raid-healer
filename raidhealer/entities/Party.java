package com.silensoftware.raidhealer.entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import com.silensoftware.raidhealer.Settings;
import com.silensoftware.raidhealer.UI;

/**
 * 
 *
 */
public class Party 
{
	 private Tank[] tanks;//All the tanks in the fight
     private Dps[] dpss;//All the dps in the fight
     private Healer healer;//One healer in the fight

     /**
      * Goes through creating the correct amount for each array
      * and positions everything based off of Settings and instantiates
      * each class. Inserts from left to right, then makes a new row once
      * the UI.rowsAcross is filled.
      */
     public Party()
     {
    	 float tankX,
    	 	tankY,
    	 	dpsX,
    	 	dpsY,
    	 	healerX,
    	 	healerY;
    	 
    	 float gap = UI.bossHealthBarHeight + UI.groupGap;
    	 
         tanks = new Tank[Settings.TankAmount];
         dpss = new Dps[Settings.DpsAmount];

         int x = 0, y = 0;
         for (int i = 0; i < Settings.TankAmount; i++)
         {
        	 tankX = UI.healthBarX + (x * UI.fullHealthBarWidth);
        	 tankY =  UI.healthBarY + (y * UI.fullHealthBarHeight) + gap;
        	 
             tanks[i] = new Tank(tankX, tankY);
             
             x++;
             if (x >= UI.rowsAcross)//Start next row
             {
                 y++;
                 x = 0;
             }
         }
         for (int i = 0; i < Settings.DpsAmount; i++)
         {
        	 dpsX = UI.healthBarX + (x * UI.fullHealthBarWidth);
        	 dpsY = UI.healthBarY + (y * UI.fullHealthBarHeight) + gap;
        	 
             dpss[i] = new Dps(dpsX, dpsY);
             
             x++;
             if (x >= UI.rowsAcross)//go down a row
             {
                 y++;
                 x = 0;
             }
         }

         healerX = UI.healthBarX + (x * UI.fullHealthBarWidth);
         healerY =  UI.healthBarY + (y * UI.fullHealthBarHeight) + gap;
         healer = new Healer(healerX, healerY);

     }

     /**
      * Called once per frame, anything that doesn't go off events should be
      * updated in this frame
      */
     public void update()
     {
         for(Tank tank: tanks)
             tank.update();

         for(Dps dps: dpss)
             dps.update();

         healer.update();
     }

     /**
      * Draw all the sprites to the screen
      * 
      * @param spriteBatch The sprite batch to render to
      */
     public void draw(SpriteBatch spriteBatch)
     {
         for(Tank tank: tanks)
             tank.draw(spriteBatch);

         for(Dps dps: dpss)
             dps.draw(spriteBatch);

         healer.draw(spriteBatch);
     }
     
     /**
      * Draw outlines around each sprite
      * 
      * @param shapeRenderer The shape renderer to render to
      */
     public void drawOutlines(ShapeRenderer shapeRenderer)
     {
         for(Tank tank: tanks)
             tank.drawOutlines(shapeRenderer);

         for(Dps dps: dpss)
             dps.drawOutlines(shapeRenderer);

         healer.drawOutlines(shapeRenderer);
     }

     /**
      * Used to get the array of tanks
      * 
      * @return An array of all tanks
      */
     public Tank[] getTanks()
     { 
         return tanks; 
     }

     /**
      * Used to get the array of dps
      * 
      * @return An array of all DPS
      */
     public Dps[] getDpss()
     {
         return dpss; 
     }

     /**
      * Used to get the healer
      * 
      * @return The healer
      */
     public Healer getHealer()
     {
         return healer;
     }

     /**
      * This method is passed in a player from another class and then that
      * player is passed to the healer
      * 
      * @param player The player the healer should heal
      */
     private void healPlayer(CharacterClass player)
     {
         healer.healPlayer(player);
     }
     
     /**
      * Put all members in an array and pass it to healer to heal
      */
     private void healAll()
     {
    	 CharacterClass[] chars = new CharacterClass[Settings.PartyAmount];
    	 int count = 0;
    	 
         for(Tank tank: tanks)
             chars[count++] = tank;
         
         for(Dps dps: dpss)
        	 chars[count++] = dps;
         
         chars[count] = healer;
         
         healer.healAll(chars);
     }
     
     /**
      * Puts a HoT on the player
      * 
      * @param player The player to put the HoT on
      */
     private void healHot(CharacterClass player)
     {
    	 healer.healHot(player);
     }
     
     /**
      * Input for mouse presses, touches etc
      * Detect what party member has been pressed (if any) and return
      * as soon as one was found. Done by checking the x/y of the press
      * and detecting whether it was in the party members hit box
      * 
      * @param x The horizontal position of the touch
      * @param y The vertical position of the touch
      * @see Libgdx documentation for touchdown
      */
     public void touchDown(int x, int y, int pointer, int button)
     {
    	 Spell[] spells = healer.getSpells();
    	 int currentSpell = healer.getCurrentSpellIndex();
    	 
    	 if(!healer.isCasting())
    	 {
	    	 for(int i = 0; i < spells.length; i++)//Check against spells
	    	 {
	    		 if(spells[i].contains(x, y))
	    		 {
	    			 healer.setActiveSpell(i);
	    			 if(spells[i].isHealAll())
	    			 {
	    				 healAll();
	    				//change it back to what it was
	    				 healer.setActiveSpell(currentSpell);
	    			 }
	    			 //no else because they are individually chosen below
	    			 return;
	    		 }
	    	 }
    	 }
    	 
         for(Tank tank: tanks)//Check against each tank
         {
             if (tank.healthBarContains(x, y) && tank.isAlive())
             {
            	 if(spells[currentSpell].isHot())
            		 healHot(tank);
            	 else
            		 healPlayer(tank);
                 return;
             }
         }
         
         for(Dps dps: dpss)//Check against each dps
         {
             if (dps.healthBarContains(x, y) && dps.isAlive())
             {
            	 if(spells[currentSpell].isHot())
            		 healHot(dps);
            	 else
            		 healPlayer(dps);
                 return;
             }
         }
         
         //check against the healer
         if(healer.healthBarContains(x, y) && healer.isAlive())
         {
        	 if(spells[currentSpell].isHot())
        		 healHot(healer);
        	 else
        		 healPlayer(healer);
        	 return;
         }
         
         if(healer.castBarContains(x, y) && healer.isCasting())
        	 healer.cancelSpell();
     }
     
     /**
      * Traverse through all party members and call their dispose methods
      */
     public void dispose()
     {
         for(Tank tank: tanks)
             tank.dispose();
         
         for(Dps dps: dpss)
             dps.dispose();

         healer.dispose();
     }

}
