package com.silensoftware.raidhealer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * These are settings that relate to the UI such as positions and width/height.
 * It could have been in a file but it's not.
 * 
 * Will need to change the math on this to work better for different ratios
 */
public class UI 
{
    //UI
    public static Color BackgroundColor = new Color(50, 50, 50, 255);//Background color of application
    public static Vector2 WindowDimension = new Vector2(400, 600);//Size of window
    public static String WindowTitle = "RaidHealer 0.01";//Title on desktop window
    public static Color HealthBarHoverColor = new Color(255, 255, 255, 100);

    //health bars
    public static int rowsAcross = 2;//How many per line
    public static float fullHealthBarWidth = Gdx.graphics.getWidth() / 2.5f;//Width of health bar in pixels
    public static float fullHealthBarHeight = Gdx.graphics.getHeight() / 12;//Height of health bar in pixels
    public static int healthBarX = Gdx.graphics.getWidth() / 10;//How far left it should start
    public static int healthBarY = Gdx.graphics.getHeight() / 15;//How far down it should start
    
    public static float groupGap = Gdx.graphics.getHeight() / 70;//Gap between boss, healthbar, castbar
    
    public static float hotIconXGap = fullHealthBarWidth / 10;//Heal over time X
    public static float hotIconWidth = fullHealthBarWidth / 8;//Heal over time width
    public static float hotIconHeight = fullHealthBarHeight / 3;//Heal over time height
    
    //healer ui
    public static float castBarWidth = fullHealthBarWidth * rowsAcross;
    public static float castBarHeight = fullHealthBarHeight / 1.5f;
    public static float manaBarWidth = fullHealthBarWidth * rowsAcross;
    public static float manaBarHeight = fullHealthBarHeight / 3;
    public static float spellWidth = castBarWidth / 5;//5 to a line
    public static float spellHeight = spellWidth;
    
    public static float bossHealthBarWidth= fullHealthBarWidth * rowsAcross;
    public static float bossHealthBarHeight = fullHealthBarHeight / 1.5f;
    
//MENUS
    //main menu
    public static int mainMenuItemAmount = 4;
    public static float mainMenuItemWidth = Gdx.graphics.getWidth() / 1.5f;//Width of item
    public static float mainMenuItemHeight =  Gdx.graphics.getHeight() / 6;//Height of item
    public static float mainMenuX = 
    		(Gdx.graphics.getWidth() - mainMenuItemWidth) / 2;//How far left it should start - should be centered
    public static float mainMenuY = 
    		(Gdx.graphics.getHeight() - mainMenuItemHeight)
    		/ (mainMenuItemAmount * 1.5f);//How far down it should start - centered
    public static int fontSize = Gdx.graphics.getWidth() / 14;
    
    //back button
    public static float backButtonWidth = Gdx.graphics.getWidth() / 3f;
    public static float backButtonHeight = Gdx.graphics.getHeight() / 12;
    public static float backButtonX = 
    		(Gdx.graphics.getWidth() - backButtonWidth) / 2;//How far left - centered
    public static float backButtonY = 
    		Gdx.graphics.getHeight() - (backButtonHeight * 3);
    
    //in game message
    public static float inGameMessageWidth = Gdx.graphics.getWidth() / 1.3f;
    public static float inGameMessageHeight = inGameMessageWidth * 1.333f;
    public static float inGameMessageX = 
    		(Gdx.graphics.getWidth() - inGameMessageWidth) / 2;//How far left - centered
    public static float inGameMessageY = Gdx.graphics.getHeight() / 6;
    public static float inGameOptionWidth = inGameMessageWidth / 2;
    public static float inGameOptionHeight =  Gdx.graphics.getHeight() / 12;
    public static float inGameMessagePadding = Gdx.graphics.getWidth() / 30;
    
    //difficulty buttons
    public static float changeWidth = Gdx.graphics.getWidth() / 8 ;
    public static float changeHeight = changeWidth;
    public static float changeDecreaseX = Gdx.graphics.getWidth() / 12;
    public static float changeIncreaseX = Gdx.graphics.getWidth() 
    		- changeDecreaseX - changeWidth;
    public static float changeY = (Gdx.graphics.getHeight() - changeHeight) / 2;
    
}
