package com.silensoftware.raidhealer.Menu;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * MainMenu is made up of a bunch of MenuItems. Simply a texture with methods
 * to render sprite and outlines, as well as whether they were pressed or not.
 */
public class MenuItem extends Sprite
{
	/**
	 * Constructor, set up variables
	 * 
	 * @param texture Texture for the menu item to use
	 * @param x Xpos of the item
	 * @param y Ypos of the item
	 * @param width Width of the item
	 * @param height Height of the item
	 */
	public MenuItem(Texture texture, float x, float y, float width, float height)
	{
		this.setTexture(texture);
		this.setPosition(x, y);//compensate for flip from camera
		this.setSize(width, height);//flip vertically
		this.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
	}
	
	/**
	 * Render sprites, called once per frame
	 * 
	 * @param spriteBatch The sprite back to render to
	 */
	public void draw(SpriteBatch spriteBatch)
	{
		spriteBatch.draw(this.getTexture(), this.getX(), this.getY(), 
				this.getWidth(), this.getHeight());
	}
	
	/**
	 * Render outlines, called once per frame
	 * 
	 * @param shapeRenderer The shape renderer to render to
	 */
	public void drawOutlines(ShapeRenderer shapeRenderer)
	{
		shapeRenderer.rect(this.getX(), this.getY(), 
				this.getWidth(), this.getHeight());
	}
	
    /**
     * Whether the box contains a point
     * Bounding box 'collision'
     * 
     * @param x The horizontal position of the point
     * @param y The vertical position of the point
     * @return Whether the points are contained within the box
     */
    public boolean contains(int x, int y)
    {
  	  return this.getX() <= x 
  			  && this.getX() + this.getWidth() >= x 
  			  && this.getY() <= y 
  			  && this.getY() + this.getHeight() >= y;
    }
    
    /**
     * Dispose of any libgdx classes that contain dispose methods
     */
    public void dispose()
    {
    	this.getTexture().dispose();
    }
}
