package com.silensoftware.raidhealer.Menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.silensoftware.raidhealer.Play;
import com.silensoftware.raidhealer.UI;

/**
 * Before entering the game this is shown. Contains different options like
 * playing the game, changing difficulty, about etc
 */
public class GameMenu 
{
	public enum MENU { 
		MAIN_MENU, 
		PLAYING, 
		LEVEL, 
		ABOUT, 
		EXIT,
		WON, 
		LOST, 
		CONFIRM_BACK };//all menu states (main menus and in game menus)
	
	BitmapFont calibrib, calibribSmall;//Two different sized fonts
	String message;//Used to display message while paused
	String noMessage = "No";//Prompt no
	String yesMessage = "Yes";//Prompt yes
	String difficultyText = "1";//The difficulty to start level
	String levelText = "Level";
	Vector2 noBounds, yesBounds, difficultyBounds;//Bounds for text
	
	private float difficultyChangeTimer = 0.18f;//How fast can change
	private float difficultyCurrentTime = 0;//Current time since last pressed
	private int aboutSelected = 0;
	private int aboutTotal = 3;
	
	public static MENU GAME_MENU;//main menu game state
	
	private boolean restart;//Whether to restart or not
	
	private Sprite background;//Background image
	private Sprite about[];
	private MenuItem[] mainMenuItems;//buttons to display on main menu
	private MenuItem backButton;//Back button to return to the main menu
	private MenuItem inGameMessage;//message to appear in game (won or lost)
	private MenuItem yesButton;//yes button to appear with inGameMessage
	private MenuItem noButton;//no button to appear with inGameMessage
	private MenuItem blackTransparent;//black overlay that covers entire screen
	private MenuItem changeIncrease, changeDecrease, difficultyPlay;
	
	/**
	 * Set up all variables that need to be running at start up
	 * This makes a few calls to other methods as there is quite a bit to
	 * setup. Essentially instantiates and positions main menu, in-game message,
	 * button positions, information in about screen etc.
	 */
	public GameMenu()
	{
		FileHandle bg = Gdx.files.internal("images/Menu/Background.png"),
			back = Gdx.files.internal("images/Menu/BackButton.png"),
			difficulty = Gdx.files.internal("images/Menu/Play.png"),
			black = Gdx.files.internal("images/Colours/BlackTransparent.png");
		
		float x = UI.backButtonX,
			y = UI.backButtonY,
			width = UI.backButtonWidth,
			height = UI.backButtonHeight;
		
		calibrib = Play.Generator.generateFont(UI.fontSize);
		calibrib.setColor(Color.WHITE);
		calibrib.setScale(1, -1);
		calibribSmall = Play.Generator.generateFont((int)(UI.fontSize / 1.2f));
		calibribSmall.setColor(Color.WHITE);
		calibribSmall.setScale(1, -1);
		
		restart = false;
		
		mainMenuItems = new MenuItem[UI.mainMenuItemAmount];
		
		background = new Sprite(new Texture(bg));
		background.setPosition(0, 0);
		background.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		//background.
		
		GAME_MENU = MENU.MAIN_MENU;
		
		setUpMainMenu();
		
		backButton = new MenuItem(new Texture(back), x, y, width, height);
		
		difficultyPlay = new MenuItem(new Texture(difficulty), x, y - height,
				width, height);
		
		setUpMessage();
		
		//Entire screen
		blackTransparent = new MenuItem(new Texture(black), 0, 0,
				Gdx.graphics.getWidth(), Gdx.graphics.getHeight());	
		
		setUpDifficulty();
		
		setUpAbout();
	}
	
	/**
	 * Called once per frame. Everything that doesnt get updated on events
	 * is updated in here.
	 */
	public void update()
	{
		difficultyCurrentTime += Gdx.graphics.getDeltaTime();
		
		//change difficulty text if pressed, time limit on change rate
		if(Gdx.input.isTouched())
		{
			if(GAME_MENU == MENU.LEVEL)
			{
				if(difficultyCurrentTime >= difficultyChangeTimer)
				{
					checkDifficultyChange();
				}
			}
		}
	}
	
	/**
	 * When a button is pressed touchDown() is called and then calls this
	 * to make a final update. Saves having to call update constantly
	 * Handling of each button is done in here
	 */
	private void buttonPressed()
	{
		switch(GAME_MENU)
		{
			case EXIT:
				Gdx.app.exit();
		}
	}
	
	/**
	 * This is called when its time to draw to the screen.
	 * See what state menu is in and draw accordingly
	 * 
	 * @param spriteBatch The sprite batch to render to
	 */
	public void draw(SpriteBatch spriteBatch)
	{
		switch(GAME_MENU)
		{
		case PLAYING:
			drawPlaying(spriteBatch);
			break;
		case MAIN_MENU:
			drawMainMenu(spriteBatch);
			break;
		case ABOUT:
			drawAbout(spriteBatch);
			break;
		case LEVEL:
			drawLevel(spriteBatch);
			break;
		case WON:
		case LOST:	
		case CONFIRM_BACK:
			drawInGameMessage(spriteBatch);
			break;
		}
	}
	
	/**
	 * Displays an in game message. Depending on whether won or lost the texture
	 * is changed elsewhere, so can just render the same thing
	 * 
	 * @param spriteBatch The sprite batch to render to
	 */
	private void drawInGameMessage(SpriteBatch spriteBatch)
	{
		float x = inGameMessage.getX() + UI.inGameMessagePadding;
		float y = inGameMessage.getY() + UI.inGameMessagePadding;
		float width = inGameMessage.getWidth() - (UI.inGameMessagePadding * 2);
		
		blackTransparent.draw(spriteBatch);
		blackTransparent.draw(spriteBatch);
		inGameMessage.draw(spriteBatch);
		calibrib.drawWrapped(spriteBatch, message, x, y, width);
		calibribSmall.draw(spriteBatch, noMessage, noBounds.x, noBounds.y);
		calibribSmall.draw(spriteBatch, yesMessage, yesBounds.x, yesBounds.y);
	}
	
	/**
	 * What to draw while playing the game
	 * draw MENU.PLAYING
	 * 
	 * @param spriteBatch The sprite batch to render to
	 */
	private void drawPlaying(SpriteBatch spriteBatch)
	{
		backButton.draw(spriteBatch);
	}
	
	/**
	 * draw MENU.MAIN_MENU
	 * 
	 * @param spriteBatch The sprite batch to render to
	 */
	private void drawMainMenu(SpriteBatch spriteBatch)
	{
		Gdx.gl.glClearColor(1f, 1f, 1f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		background.draw(spriteBatch);
		
		for(MenuItem item: mainMenuItems)
		{
			item.draw(spriteBatch);
		}
	}
	
	/**
	 * draw MENU.ABOUT
	 * 
	 * @param spriteBatch The sprite batch to render to
	 */
	private void drawAbout(SpriteBatch spriteBatch)
	{
		float x = inGameMessage.getX() + UI.inGameMessagePadding;
		float y = inGameMessage.getY() + UI.inGameMessagePadding;
		float width = inGameMessage.getWidth() - (UI.inGameMessagePadding * 2);
		
		Gdx.gl.glClearColor(0f/255, 65f/255, 55f/255, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		about[aboutSelected].draw(spriteBatch);
		backButton.draw(spriteBatch);
		if(aboutSelected < aboutTotal - 1)
			changeIncrease.draw(spriteBatch);
		if(aboutSelected > 0)
			changeDecrease.draw(spriteBatch);
	}
	
	/**
	 * draw MENU.LEVEL
	 * 
	 * @param spriteBatch The sprite batch to render to
	 */
	private void drawLevel(SpriteBatch spriteBatch)
	{
		float x = difficultyBounds.x,
			y= difficultyBounds.y,
			diffH = calibrib.getBounds(difficultyText).height,
			levelW = calibrib.getBounds(levelText).width,
			levelX = (Gdx.graphics.getWidth() / 2) - (levelW / 2);
		
		Gdx.gl.glClearColor(0f/255, 65f/255, 55f/255, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		calibrib.draw(spriteBatch, difficultyText, x, y);
		backButton.draw(spriteBatch);
		if(Integer.parseInt(difficultyText) < Integer.MAX_VALUE)//max level
			changeIncrease.draw(spriteBatch);
		if(Integer.parseInt(difficultyText) > 1)//min level
			changeDecrease.draw(spriteBatch);
		difficultyPlay.draw(spriteBatch);
		calibrib.draw(spriteBatch, levelText, levelX, y + (diffH * 2));
	}
	
	/**
	 * Draw outlines for shapes
	 * 
	 * @param shapeRenderer The shape renderer to render to
	 */
	public void drawOutlines(ShapeRenderer shapeRenderer)
	{
		for(MenuItem item: mainMenuItems)
		{
			item.drawOutlines(shapeRenderer);
		}
	}
	
	/**
	 * Set up the bounds for in game messages, uses position of other items
	 * to position everything according to each other
	 */
	private void setUpMessage()
	{
		float x = UI.inGameMessageX,
			y = UI.inGameMessageY,
			msgW = UI.inGameMessageWidth,
			msgH = UI.inGameMessageHeight,
			optionW = UI.inGameOptionWidth,
			optionH = UI.inGameOptionHeight,
			width, height, textW;
		
		FileHandle handle = Gdx.files.internal("images/Message.png");

		inGameMessage = new MenuItem(new Texture(handle),x, y, msgW, msgH);
		
		handle = Gdx.files.internal("images/Colours/NoTexture.png");
		y = y + msgH - UI.inGameOptionHeight;//Place at bottom
		
		noButton = new MenuItem(new Texture(handle), x, y, optionW, optionH);
		
		handle = Gdx.files.internal("images/Colours/NoTexture.png");
		x = x + optionW;
		
		yesButton = new MenuItem(new Texture(handle), x, y, optionW, optionH);

		width = noButton.getWidth();
		height = noButton.getHeight();
		textW =calibribSmall.getBounds(noMessage).width;
		x = noButton.getX() + ((width - textW) / 2);
		y = noButton.getY() + ((height - textW) / 2);
		
		noBounds = new Vector2(x, y);
		
		width = yesButton.getWidth();
		height = yesButton.getHeight();
		textW = calibribSmall.getBounds(yesMessage).width;
		x = yesButton.getX() + ((width - textW) / 2);
		y = yesButton.getY() + ((height - textW) / 2);
		
		yesBounds = new Vector2(x, y);
	}
	
	/**
	 * Set up the sprites and positioning of graphics
	 */
	private void setUpDifficulty()
	{
		float screenW = Gdx.graphics.getWidth(),
			screenH = Gdx.graphics.getHeight(),
			textW = calibrib.getBounds(difficultyText).width,
			width = (screenW - textW) / 2,
			height = (screenH - textW) / 2,
			decreaseX = UI.changeDecreaseX,
			increaseX = UI.changeIncreaseX,
			changeY = UI.changeY,
			changeW = UI.changeWidth,
			changeH = UI.changeHeight;
		
		FileHandle handle = Gdx.files.internal("images/Menu/Right.png");
		difficultyBounds = new Vector2(width, height);
		
		changeIncrease = new MenuItem(
				new Texture(handle), increaseX, changeY, changeW, changeH);
		
		handle = Gdx.files.internal("images/Menu/Left.png");
		
		changeDecrease = new MenuItem(
				new Texture(handle),decreaseX, changeY, changeW, changeH);
	}
	
	/**
	 * Set up everything in the about section
	 */
	private void setUpAbout()
	{
		float x = UI.inGameMessageX,
			msgW = UI.inGameMessageWidth,
			msgH = UI.inGameMessageHeight;
		
		FileHandle boss = Gdx.files.internal("images/Menu/About/Boss.png"),
			party = Gdx.files.internal("images/Menu/About/Party.png"),
			spells = Gdx.files.internal("images/Menu/About/Spells.png"),
			temp = boss;
		
		about = new Sprite[aboutTotal];
		aboutSelected = 0;
		
		for(int i = 0; i < aboutTotal; i++)
		{
			switch(i)
			{
			case 0: temp = boss; break;
			case 1: temp = party; break;
			case 2: temp = spells; break;
			}
			about[i] = new Sprite(new Texture(temp));
			about[i].setPosition(x, x);//distance from top and left are same
			about[i].setSize(msgW, msgH);
			about[i].getTexture().setFilter(TextureFilter.Linear, 
					TextureFilter.Linear); //make it draw properly
		}
	}
	
	/**
	 * Loop through and set up each menu item in main menu
	 */
	private void setUpMainMenu()
	{
		Texture texture;
		String location = "";
		
		float x = UI.mainMenuX, 
				w = UI.mainMenuItemWidth,
				h = UI.mainMenuItemHeight,
				y;//Changes each time
		
		for(int i = 0; i < mainMenuItems.length; i++)
		{
			switch(i)
			{
			case 0:
				location = "images/Menu/Play.png";
				break;
			case 1:
				location = "images/Menu/Level.png";
				break;
			case 2:
				location = "images/Menu/About.png";
				break;	
			case 3:
				location = "images/Menu/Exit.png";
				break;
			default:
				location = "images/NoTexture.png";
			}
			
			texture = new Texture(Gdx.files.internal(location));
			y = UI.mainMenuY + (i * UI.mainMenuItemHeight);
			
			mainMenuItems[i] = new MenuItem(texture, x, y, w, h);
		}
	}
	
	/**
	 * When user presses something this is called
	 */
	public void touchDown(int screenX, int screenY, int pointer, int button) 
	{
		switch(GAME_MENU)
		{
		case MAIN_MENU:
			for(int i = 0; i < mainMenuItems.length; i++)
			{
				if(mainMenuItems[i].contains(screenX, screenY))
				{
					switch(i)
					{
					case 0:
						GAME_MENU = MENU.PLAYING;//first button
						restart = true;
						break;
					case 1:
						changeIncrease.setY(UI.changeY);//move position
						changeDecrease.setY(UI.changeY);//move position
						GAME_MENU = MENU.LEVEL;
						break;
					case 2:
						//shift arrows down below images
						changeIncrease.setY(backButton.getY());
						changeDecrease.setY(backButton.getY());
						GAME_MENU = MENU.ABOUT;
						break;
					case 3:
						GAME_MENU = MENU.EXIT;
					}
					buttonPressed();
				}
			}
			break;
		case LEVEL:
			checkDifficultyChange();
			if(difficultyPlay.contains(screenX, screenY))
			{
				GAME_MENU = MENU.PLAYING;
				restart = true;
			}
			//no break as want it to go into next statement for back button
		case ABOUT:
			checkAboutChange();
			if(backButton.contains(screenX, screenY))//back button pressed
			{
				GAME_MENU = MENU.MAIN_MENU;
				difficultyText = "1";//reset back to 1
			}
			break;
		case PLAYING:
			if(backButton.contains(screenX, screenY))//back button pressed
			{
				GAME_MENU = MENU.CONFIRM_BACK;
				confirmBack();
			}
			break;
		case CONFIRM_BACK:
			if(noButton.contains(screenX, screenY))//the no button pressed
				GAME_MENU = MENU.PLAYING;
			else if(yesButton.contains(screenX, screenY))//yes button pressed
				GAME_MENU = MENU.MAIN_MENU;
			break;
		case WON:
			if(noButton.contains(screenX, screenY))//the no button was pressed
				GAME_MENU = MENU.MAIN_MENU;
			else if(yesButton.contains(screenX, screenY))//yes button pressed
			{
				GAME_MENU = MENU.PLAYING;
				restart = true;
				nextDifficulty();
			}
			break;
		case LOST:
			if(noButton.contains(screenX, screenY))//if the no button was pressed
				GAME_MENU = MENU.MAIN_MENU;
			else if(yesButton.contains(screenX, screenY))//if the yes button was pressed
			{
				GAME_MENU = MENU.PLAYING;
				restart = true;
			}
			break;
		}
	}
	
	/**
	 * Increase the difficulty by one. Called after the player has won the 
	 * fight and wants to go to the next difficulty
	 */
	private void nextDifficulty()
	{
		float textW;
		int current = Integer.parseInt(difficultyText);//convert current to int
		
		current ++;//add the new amount (-1 or +1)
		if(current < 1)//dont let it go less than 1
			current = 1;
		
		difficultyText = Integer.toString(current);//change text to new int
		textW = calibribSmall.getBounds(difficultyText).width;
		
		difficultyBounds.x = (Gdx.graphics.getWidth() - textW) / 2;
	}
	
	/**
	 * Check to see whether the user increased/decreased the about page
	 */
	private void checkAboutChange()
	{
		int x = Gdx.input.getX();
		int y = Gdx.input.getY();
		int amount = 0;
		float textW;
		
		if(changeIncrease.contains(x, y))
			 amount = 1;
		else if(changeDecrease.contains(x, y))
			amount = -1;
		
		if(amount != 0)
		{
			aboutSelected += amount;//add the new amount (-1 or +1)
			if(aboutSelected < 0)//dont let it go less than 1
				aboutSelected = 0;
			if(aboutSelected > aboutTotal - 1)
				aboutSelected = aboutTotal - 1;
			
			difficultyCurrentTime = 0;
		}
	}
	
	/**
	 * Check to see whether the user increased/decreased the difficulty
	 */
	private void checkDifficultyChange()
	{
		int x = Gdx.input.getX();
		int y = Gdx.input.getY();
		int amount = 0;
		float textW;
		
		if(changeIncrease.contains(x, y))
			 amount = 1;
		else if(changeDecrease.contains(x, y))
			amount = -1;
		
		if(amount != 0)
		{
			int current = Integer.parseInt(difficultyText);//string to int
			if(current == Integer.MAX_VALUE && amount == 1)//stop overflow
				return;
			current += amount;//add the new amount (-1 or +1)
			if(current < 1)//dont let it go less than 1
				current = 1;
			
			difficultyText = Integer.toString(current);//change text to new int
			textW = calibribSmall.getBounds(difficultyText).width;
			
			difficultyBounds.x = (Gdx.graphics.getWidth() - textW) / 2;
			
			difficultyCurrentTime = 0;
		}
	}
	
	/**
	 * Dispose of any libgdx classes that have a dispose method
	 */
	public void dispose()
	{
		for(MenuItem item: mainMenuItems)
			item.dispose();
		for(Sprite s: about)
			s.getTexture().dispose();
		backButton.dispose();
		inGameMessage.dispose();
		yesButton.dispose();
		noButton.dispose();
		blackTransparent.dispose();
		changeIncrease.dispose();
		changeDecrease.dispose();
		difficultyPlay.dispose();
	}
	
	/**
	 * Set the message to a confirmation message
	 */
	public void confirmBack()
	{
		message = "Are you sure you want to return to the menu?"
				+ " The current fight will be lost.";
	}
	
	/**
	 * This is called when the player wins the fight
	 */
	public void playerWon()
	{
		GAME_MENU = MENU.WON;
		message = "Congratulations, you beat level " + difficultyText + "! "
				+ "Do you want to go to the next level?";
	}
	
	/**
	 * This is called when the player losses the fight
	 */
	public void playerLost()
	{
		GAME_MENU = MENU.LOST;
		message = "You lost! Do you want to restart the fight?";
	}
	
	/**
	 * This is called from Play when the fight has been restarted
	 */
	public void restarted()
	{
		restart = false;
	}
	
	/**
	 * Get the difficulty of the game
	 * @return The current difficulty
	 */
	public int getDifficulty()
	{
		return Integer.parseInt(difficultyText);
	}
	
	/**
	 * Whether the game should restart or not
	 * @return Whether to restart or not
	 */
	public boolean shouldRestart()
	{
		return restart;
	}
}
