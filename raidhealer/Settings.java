package com.silensoftware.raidhealer;

/**
 * This is the settings class. It could have been in a file but it's not.
 * This deals with any balancing such as how much damage a class should 
 * take or deal, health amounts, size of party etc.
 */
public class Settings 
{
    //RAID MAKE UP
    public static int BossAmount = 1;//Bosses in fight
    public static int TankAmount = 2;//Tanks in party
    public static int DpsAmount = 7;//Dps in party
    public static int HealerAmount = 1;//Amount of healers (1 because its the player)
    public static int PartyAmount = TankAmount + DpsAmount + HealerAmount;//Total party members

    //HEALTH
    public static int BossHealth = 6000000;//Boss health - modified by Fight class
    public static int TankHealth = 10000;//Each tanks health - 100k
    public static int DpsHealth = 10000;//Each dps health - 10k
    public static int HealerHealth = 15000;//Each healers health - 15k
    public static float TankDamageTakenModifier = 2.0f;//How much (times) less tank soaks
    public static int HealthRegen = 50;//Health gained per second

    //DAMAGE DEALT
    public static int DamageDealtPerSecond = 5000;//Damage from dps
    public static float TankDamageDealtModifier = 0.2f;//How much less tank deals

    //PARTY
    public static int HitPartyOutOf = 10;//Chance out of ? to hit a party member
    public static int HitPartyChance = 1;//This/HitPartyOutOf
    public static float SwingTimer = 1f;//Swing timer, in seconds
    
    //HEALER RELATED
    public static float HealerMaxMana = 100;//Maximum mana the healer has
    public static float HealerManaRegen = 2f;//Mana per second gained
}
